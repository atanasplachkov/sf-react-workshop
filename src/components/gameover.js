import React from "react";
import {Link} from "react-router";

export default class extends React.Component {
	render() {
		return (
			<div className="game-over">
				<h1>Game Over</h1>
				<div className="center">
					<Link to="highscores">Continue</Link>
				</div>
			</div>
		);
	}
};
