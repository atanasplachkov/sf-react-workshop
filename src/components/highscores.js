import React from "react";
import {connect} from "react-redux";
import {Link} from "react-router";

import dispatcher from "../dispatcher";
import "./sass/highscores.scss";

class HighScores extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			score: this.props.score,
			scoresList: "",
			shouldEnterName: false,
			name: ""
		};

		this.onNameChange = this.onNameChange.bind(this);
		this.submitScore = this.submitScore.bind(this);
	}

	componentWillMount() {
		if (!this.state.score) {
			this._handleResponse(fetch("/api/highscores"));
		} else {
			this.state.shouldEnterName = true;
			dispatcher.emit(dispatcher.ACTIONS.CLEAR_SCORE);
		}
	}

	render() {
		let content;

		if (this.state.shouldEnterName) {
			content = (
				<div>
					<input value={this.state.name} onChange={this.onNameChange} placeholder="Enter your name"/>
					&nbsp;
					<button onClick={this.submitScore}>Submit</button>
					<br/>
					<br/>
				</div>
			);
		} else {
			content = <div dangerouslySetInnerHTML={{__html: this.state.scoresList}} />;
		}

		return (
			<div className="high-scores">
				<h1>Highscores</h1>
				{content}
				<div className="nav">
					<Link to="menu">Go back</Link>
				</div>
			</div>
		);
	}

	onNameChange(e) {
		this.setState({name: e.currentTarget.value});
	}

	submitScore() {
		this.state.shouldEnterName = false;
		this._handleResponse(fetch("/api/highscores", {
			method: "post",
			body: JSON.stringify({
				name: this.state.name,
				score: this.state.score
			})
		}));
	}

	_handleResponse(promise) {
		promise
			.then((data) => data.text())
			.then((list) => this.setState({scoresList: list}));
	}
}

HighScores.displayName = "HighScores";

HighScores.propTypes = {
	score: React.PropTypes.number
};

export default connect((state) => {
	return {
		score: state.get("score")
	};
})(HighScores);
