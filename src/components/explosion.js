import React from "react";

class Explosion extends React.Component {
	render() {
		return (
			<div className="explosion" style={this.props.style} />
		);
	}
}

Explosion.displayName = "Explosion";
Explosion.propTypes = {
	style: React.PropTypes.shape({
		width: React.PropTypes.number,
		height: React.PropTypes.number,
		backgroundPosition: React.PropTypes.string
	}).isRequired
};

Explosion.FRAMES = 48;
Explosion.TRANSITION_TIME = 0.015;

export default Explosion;
