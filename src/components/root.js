import React from "react";
import {Provider} from "react-redux";
import {browserHistory, Router} from "react-router";

import store from "../store";

import App from "./app";
import Menu from "./menu";
import Game from "./game";
import HighScores from "./highscores";

const routes = [
	{
		path: "/",
		component: App,
		indexRoute: {
			onEnter: (nextState, replace) => replace("/menu")
		},
		childRoutes: [
			{
				path: "menu",
				component: Menu
			},
			{
				path: "game",
				component: Game
			},
			{
				path: "highscores",
				component: HighScores
			}
		]
	}
];

export default class extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<Router history={browserHistory} routes={routes}/>
			</Provider>
		);
	}
};
