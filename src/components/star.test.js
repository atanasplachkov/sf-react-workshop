import React from "react";
import Star from "./star";
import renderer from "react-test-renderer";

describe("Star style", () => {
	test("Changes X and Y", () => {
		const star = {
			x: 0,
			y: 0,
			type: 0,
			firstFrame: 0
		};
		const component = renderer.create(<Star star={star} time={0} />);

		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();

		star.x = 100;
		tree.props.star = star;
		tree = component.toJSON();
		expect(tree).toMatchSnapshot();

		star.y = 100;
		tree.props.star = star;
		tree = component.toJSON();
		expect(tree).toMatchSnapshot();

		star.firstFrame = 1;
		tree.props.star = star;
		tree = component.toJSON();
		expect(tree).toMatchSnapshot();

		star.type = 2;
		tree.props.star = star;
		tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});

	test("Changes time", () => {
		const star = {
			x: 0,
			y: 0,
			type: 0
		};
		const component = renderer.create(
			<Star star={star} time={0} />
		);
		let tree = component.toJSON();
		expect(tree).toMatchSnapshot();

		tree.props.time = 100;
		tree = component.toJSON();
		expect(tree).toMatchSnapshot();

		tree.props.time = 200;
		tree = component.toJSON();
		expect(tree).toMatchSnapshot();

		tree.props.time = 300;
		tree = component.toJSON();
		expect(tree).toMatchSnapshot();
	});
});
