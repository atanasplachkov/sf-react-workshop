import React from "react";

import hoc from "./hoc";

class Enemy extends React.Component {
	render() {
		return (
			<div className="enemy" style={this.props.style}></div>
		);
	}
}

Enemy.propTypes = {
	style: React.PropTypes.shape({
		width: React.PropTypes.number,
		height: React.PropTypes.number,
		backgroundPosition: React.PropTypes.string
	}).isRequired
};

Enemy.FRAMES = 5;
Enemy.TRANSITION_TIME = 0.1;

export default hoc(Enemy);
