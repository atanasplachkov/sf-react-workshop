import React from "react";
import "./sass/fps.scss";

class FPSCounter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fps: this.props.fps
        };
    }

    componentWillReceiveProps(nextProps) {
        const state = this.state;

        if (state.fps === 0) {
            state.fps = nextProps.fps;
        }

        if (nextProps.fps > state.fps) {
            state.fps *= 1.005;
        } else if (nextProps.fps < state.fps) {
            state.fps *= 0.995;
        }
    }

    render() {
        return (
            <div className="fps-counter">
                {this.state.fps << 0}
            </div>
        );
    }
}

FPSCounter.displayName = "FPS Counter";
FPSCounter.propTypes = {
    fps: React.PropTypes.number
};

export default FPSCounter;
