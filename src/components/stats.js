import React from "react";
import {connect} from "react-redux";

class Stats extends React.Component {
	render() {
		return (
			<div className="stats">
				<div>Destroyed: {this.props.destroyedEnemies}</div>
				<div>Passed: {this.props.passedEnemies}</div>
				<div>Fired: {this.props.rocketsFired}</div>
			</div>
		);
	}
}

Stats.displayName = "Stats";
Stats.propTypes = {
	destroyedEnemies: React.PropTypes.number,
	passedEnemies: React.PropTypes.number,
	rocketsFired: React.PropTypes.number,
};

export default connect((state) => {
	const destroyedEnemies = state.get("destroyedEnemies");
	const passedEnemies = state.get("passedEnemies");
	const rocketsFired = state.get("rocketsFired");

	return {destroyedEnemies, passedEnemies, rocketsFired};
})(Stats);
