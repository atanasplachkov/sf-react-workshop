import React from "react";
import {connect} from "react-redux";
import "./sass/app.scss";

import FPSCounter from "./fps";
import Star from "./star";
import starsCreator from "../starsCreator";
import dispatcher from "../dispatcher";

class App extends React.Component {
	constructor() {
		super();

		if (module.hot) {
			module.hot.dispose(() => {
				cancelAnimationFrame(this.state.requestId);
			});
		}

		this.state = {
			requestId: null,
			fps: 0,
			stars: starsCreator(20)
		};

		this.ticker = this.ticker.bind(this);
	}

	componentDidMount() {
		this.ticker();
	}

	getChildContext() {
		return {
			gameTime: this.props.gameTime
		};
	}

	render() {
		let stars = this.state.stars;
		let renderStars = [];

		for (let i = 0, len = stars.length; i < len; i++) {
			renderStars.push(
				<Star key={i} time={this.props.appTime} star={stars[i]} />
			);
		}

		return (
			<div className="container">
				{renderStars}
				{this.props.children}
				<FPSCounter fps={this.state.fps} />
			</div>
		);
	}

	ticker() {
		let now = Date.now();
		let delta = (now - this.props.lastFrameTime) / 1000;	// seconds

		this.state.fps = ((1 / delta) << 0);
		dispatcher.emit(dispatcher.ACTIONS.TICK, {
			delta: delta,
			lastFrameTime: now
		});

		this.state.requestId = requestAnimationFrame(this.ticker);
	}
}

App.displayName = "App";
App.childContextTypes = {
	gameTime: React.PropTypes.number
};
App.propTypes = {
	gameTime: React.PropTypes.number,
	appTime: React.PropTypes.number.isRequired,
	lastFrameTime: React.PropTypes.number.isRequired
};

export default connect((state) => {
	return {
		gameTime: state.get("gameTime"),
		appTime: state.get("appTime"),
		lastFrameTime: state.get("lastFrameTime")
	};
})(App);
