import React from "react";
import {Map} from "immutable";

import Explosion from "./explosion";
import "./sass/hoc.scss";

export default function (WrappedComponent) {
	let HOC = class extends React.Component {
		render() {
			const model = this.props.model;
			const movement = model.get("movement");
			const size = model.get("size") * window.innerWidth;
			const x = (window.innerWidth * model.get("x")) - (size / 2);
			const y = (window.innerHeight * model.get("y")) - (size / 2);
			const destructionTime = model.get("destructionTime");

			const style = {
				left: x,
				top: y
			};

			let frame;
			let row;
			let classNames;
			let Component;
			let wcStyle = {
				width: size,
				height: size
			};

			if (destructionTime === true) {
				return null;
			} else if (destructionTime) {
				Component = Explosion;
			} else {
				Component = WrappedComponent;
			}

			frame = (this.context.gameTime / Component.TRANSITION_TIME % Component.FRAMES) << 0;
			if (frame > 8) {
				row = (frame / 8) << 0;
				if (row > 0) {
					frame %= 8;
				}
			} else {
				row = 0;
			}

			wcStyle.backgroundPosition = `${size * frame}px ${size * row}px`;

			classNames = "hoc";
			if (movement && !this.context.isPaused) {
				if (movement < 0) {
					classNames += " move-left";
				} else {
					classNames += " move-right";
				}
			}


			return (
				<div className={classNames} style={style}>
					<Component movement={movement} style={wcStyle} />
				</div>
			);
		}
	};

	HOC.contextTypes = {
		gameTime: React.PropTypes.number,
		isPaused: React.PropTypes.bool
	};

	HOC.propTypes = {
		model: React.PropTypes.instanceOf(Map)
	};

	return HOC;
};
