import React from "react";

import hoc from "./hoc";

class Player extends React.Component {
	render() {
		return (
			<div className="player" style={this.props.style}></div>
		);
	}
}

Player.propTypes = {
	movement: React.PropTypes.number,
	style: React.PropTypes.shape({
		width: React.PropTypes.number,
		height: React.PropTypes.number,
		backgroundPosition: React.PropTypes.string
	}).isRequired
};

Player.FRAMES = 8;
Player.TRANSITION_TIME = 0.1;

export default hoc(Player);
