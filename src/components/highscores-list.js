import React from "react";

class HighScoresList extends React.Component {
	render() {
		return (
			<ol className="high-scores-list">
				{this.props.scores.map((item, i) => {
					return (
						<li key={i}>
							<span className="name">{item.name}</span>
							<span className="score">{item.score}</span>
						</li>
					);
				})}
			</ol>
		);
	}
}

HighScoresList.displayName = "HighScoresList";
HighScoresList.propTypes = {
	scores: React.PropTypes.arrayOf(React.PropTypes.shape({
		name: React.PropTypes.string,
		score: React.PropTypes.number
	}))
};

export default HighScoresList;
