import React from "react";
import {Link} from "react-router";
import "./sass/menu.scss";

class Menu extends React.Component {
    constructor() {
        super();

        this.onClose = this.onClose.bind(this);
    }

    render() {
        if (this.props.onResume) {
            return (
                <ul className="menu">
                    <li onClick={this.props.onResume}>Resume</li>
                    <li>
                        <Link to="menu">Exit</Link>
                    </li>
                </ul>
            );
        } else {
            return (
                <ul className="menu">
                    <li><Link to="game">Start</Link></li>
                    <li><Link to="highscores">High scores</Link></li>
                    <li onClick={this.onClose}>Exit</li>
                </ul>
            );
        }
    }

    onClose() {
        window.open("about:blank", "_self");
        window.close();
    }
}

Menu.displayName = "Menu";

Menu.propTypes = {
    onResume: React.PropTypes.func
};

export default Menu;
