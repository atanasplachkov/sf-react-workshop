import React from "react";

import hoc from "./hoc";

class Rocket extends React.Component {
	render() {
		return (
			<div className="rocket" style={this.props.style} />
		);
	}
}

Rocket.propTypes = {
	style: React.PropTypes.shape({
		width: React.PropTypes.number,
		height: React.PropTypes.number
	}).isRequired
};

Rocket.FRAMES = 0;
Rocket.TRANSITION_TIME = 0;

export default hoc(Rocket);
