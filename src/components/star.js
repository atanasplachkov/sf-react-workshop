import React from "react";

import "./sass/star.scss";

const FRAME_COUNT = 8;
const STAR_SIZE = 0.017;
const TRANSITION_TIME = 0.15;

class Star extends React.Component {
	render() {
		const star = this.props.star;
		let x = star.x;
		let y = star.y;
		let type = star.type;
		let firstFrame = star.firstFrame;
		let frame = firstFrame + ((this.props.time / TRANSITION_TIME % FRAME_COUNT) << 0) - FRAME_COUNT;

		const size = STAR_SIZE * window.innerWidth;
		const style = {
			left: (window.innerWidth * x) - (size / 2),
			top: (window.innerHeight * y) - (size / 2),
			width: size,
			height: size,
			backgroundPosition: `${size * frame}px ${size * type}px`
		};

		return (
			<div className="star" style={style} />
		);
	}
}

Star.displayName = "Star";

Star.propTypes = {
	time: React.PropTypes.number,
	star: React.PropTypes.shape({
		x: React.PropTypes.number,
		y: React.PropTypes.number,
		type: React.PropTypes.number,
		firstFrame: React.PropTypes.number
	})
};

export default Star;
