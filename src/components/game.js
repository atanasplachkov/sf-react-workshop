import React from "react";
import {connect} from "react-redux";

import dispatcher from "../dispatcher";

import Menu from "./menu";
import Stats from "./stats";
import GameOver from "./gameover";
import Player from "./player";
import Enemy from "./enemy";
import Rocket from "./rocket";
import "./sass/game.scss";

const ARROW_LEFT = 37;
const ARROW_RIGHT = 39;
const SPACE = 32;
const ESCAPE = 27;

class Game extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			timerId: -1,
			wave: 1,
			showMenu: false
		};

		this.onGameRef = this.onGameRef.bind(this);
		this.onKeyDown = this.onKeyDown.bind(this);
		this.onKeyUp = this.onKeyUp.bind(this);
		this.onResume = this.onResume.bind(this);
	}

	getChildContext() {
		return {
			isPaused: this.state.showMenu
		};
	}

	componentWillMount() {
		dispatcher.emit(dispatcher.ACTIONS.GAME_START);
	}

	componentDidMount() {
		this.spawnEnemies();
	}

	componentWillUnmount() {
		dispatcher.emit(dispatcher.ACTIONS.GAME_QUIT);
	}

	render() {
		const state = this.context.store.getState();
		const gameOver = state.get("gameOver");

		if (gameOver) {
			clearTimeout(this.state.timerId);
			return (
				<GameOver />
			);
		}

		const player = state.get("player");
		const enemies = state.get("enemies");
		const rockets = state.get("rockets");
		const renderEnemies = [];
		const renderRockets = [];
		let overlay;

		for (let i = 0, len = enemies.size; i < len; i++) {
			let enemy = enemies.get(i);
			renderEnemies.push(
				<Enemy key={enemy.get("id")} model={enemy} />
			);
		}

		for (let i = 0, len = rockets.size; i < len; i++) {
			let rocket = rockets.get(i);
			renderRockets.push(
				<Rocket key={rocket.get("id")} model={rocket} />
			);
		}

		if (this.state.showMenu) {
			overlay = <Menu onResume={this.onResume}/>;
		} else {
			overlay = <Stats />;
		}

		return (
			<div ref={this.onGameRef} className="game-container" tabIndex="0" onKeyDown={this.onKeyDown} onKeyUp={this.onKeyUp}>
				{renderRockets}
				<Player model={player} />
				{renderEnemies}
				{overlay}
			</div>
		);
	}

	onGameRef(ref) {
		if (ref) {
			ref.focus();
		}
	}

	onKeyDown(e) {
		switch (e.keyCode) {
			case ARROW_LEFT:
				dispatcher.emit(dispatcher.ACTIONS.PLAYER_MOVE_LEFT);
				break;
			case ARROW_RIGHT:
				dispatcher.emit(dispatcher.ACTIONS.PLAYER_MOVE_RIGHT);
				break;
			case SPACE:
				dispatcher.emit(dispatcher.ACTIONS.PLAYER_SHOOT);
				break;
			case ESCAPE:
				this.state.showMenu = !this.state.showMenu;
				dispatcher.emit(dispatcher.ACTIONS.PLAY_PAUSE);

				if (this.state.showMenu) {
					clearTimeout(this.state.timerId);
				} else {
					this.spawnEnemies();
				}
				break;
		}
	}

	onKeyUp(e) {
		switch (e.keyCode) {
			case ARROW_LEFT:
			case ARROW_RIGHT:
				dispatcher.emit(dispatcher.ACTIONS.PLAYER_MOVE_IDLE);
				break;
			case SPACE:
				dispatcher.emit(dispatcher.ACTIONS.PLAYER_SHOOT_IDLE);
				break;
		}
	}

	onResume() {
		this.state.showMenu = false;
		dispatcher.emit(dispatcher.ACTIONS.PLAY_PAUSE);
		this.spawnEnemies();
	}

	spawnEnemies() {
		const timeout = ((Math.random() * 4) + 1) * 1000 << 0;
		this.state.timerId = setTimeout(() => {
			dispatcher.emit(dispatcher.ACTIONS.SPAWN_ENEMY, this.state.wave << 0);
			this.state.wave += 0.15;
			this.spawnEnemies();
		}, timeout);
	}
}

Game.contextTypes = {
	store: React.PropTypes.object
};

Game.childContextTypes = {
	isPaused: React.PropTypes.bool
};

export default Game;
