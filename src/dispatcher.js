import ACTIONS from "./store/actions";
import store from "./store";

class Dispatcher {
	emit(type, data) {
		store.dispatch({
			type: type,
			data: data
		});
	}

	emitDelayed(type, data) {
		setTimeout(() => this.emit(type, data), 0);
	}
}

const dispatcher = new Dispatcher();
dispatcher.ACTIONS = ACTIONS;

export default dispatcher;
