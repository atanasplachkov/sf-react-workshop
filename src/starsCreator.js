const TYPES_COUNT = 4;
const FRAME_COUNT = 8;

export default function (starsCount) {
	let stars = [];

	for (let i = 0; i < starsCount; i++) {
		stars.push({
			x: (Math.random() * 0.9) + 0.05,
			y: (Math.random() * 0.9) + 0.05,
			type: (Math.random() * TYPES_COUNT) << 0,
			firstFrame: (Math.random() * FRAME_COUNT) << 0
		});
	}

	return stars;
};
