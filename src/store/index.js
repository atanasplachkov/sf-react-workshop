import {createStore} from "redux";
import {Map} from "immutable";

import reducer from "./reducer";

const INITIAL_OPTIONAL_STATE = Map({
	lastFrameTime: Date.now(),
	appTime: 0,
	paused: true,
});

const store = createStore(reducer, INITIAL_OPTIONAL_STATE);

export default store;
