export default {
	TICK: "tick",
	PLAY_PAUSE: "play.pause",
	GAME_START: "game.start",
	GAME_QUIT: "game.quit",
	GAME_OVER: "game.over",
	CLEAR_SCORE: "clear.score",

	PLAYER_MOVE_IDLE: "player.idle",
	PLAYER_MOVE_LEFT: "player.move.left",
	PLAYER_MOVE_RIGHT: "player.move.right",
	PLAYER_SHOOT: "player.shoot",
	PLAYER_SHOOT_IDLE: "player.shoot.idle",

	SPAWN_ENEMY: "spawn.enemy"
};
