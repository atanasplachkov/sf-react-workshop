import ACTIONS from "./actions";
import {Map, List} from "immutable";

import Explosion from "../components/explosion";
import dispatcher from "../dispatcher";

const EXPLOSION_DURATION = Explosion.FRAMES * Explosion.TRANSITION_TIME;
const PLAYER_SIZE = 0.06;
const PLAYER_HALF_SIZE = PLAYER_SIZE / 2;
const ENEMY_SIZE = 0.04;
const ENEMY_HALF_SIZE = ENEMY_SIZE / 2;
const ROCKET_SIZE = 0.01;
const ROCKET_HALF_SIZE = ROCKET_SIZE / 2;
const ROCKET_SPEED = 0.8;
const NOT_MOVING = 0;
const PLAYER_SPEED = 0.3;
const ENEMY_SPEED = 0.15;
const SHOOT_TIMEOUT = 0.2;

let enemyId = 0;
let rocketId = 0;
let clearEnemies = {};
let clearRockets = {};

let lastCleanUpTime = 0;
const CLEAN_UP_INTERVAL = 1.5;

export default function (state, action) {
	const data = action.data;

	switch (action.type) {
		case ACTIONS.PLAY_PAUSE:
			return state.set("paused", !state.get("paused"));
		case ACTIONS.TICK:
			return state.withMutations((state) => {
				const delta = data.delta;

				// GENERAL
				state.set("lastFrameTime", data.lastFrameTime);
				state.set("appTime", state.get("appTime") + delta);

				if (state.get("paused")) {
					return
				}

				const gameTime = state.get("gameTime") + delta;
				state.set("gameTime", gameTime);

				// PLAYER
				const player = state.get("player");
				let rockets = state.get("rockets");
				let enemies = state.get("enemies");

				const destructionTime = player.get("destructionTime");
				if (destructionTime) {
					if (destructionTime + EXPLOSION_DURATION <= gameTime) {
						state.setIn(["player", "destructionTime"], true);
						dispatcher.emitDelayed(dispatcher.ACTIONS.GAME_OVER);
					}
				} else {
					const movement = player.get("movement");
					if (movement) {
						const x = player.get("x");
						const step = PLAYER_SPEED * delta * movement;
						const change = x + step;

						if (change > PLAYER_HALF_SIZE && change < 1 - PLAYER_HALF_SIZE) {
							state.setIn(["player", "x"], change);
						}
					}
				}

				// ROCKETS
				rockets = rockets.withMutations((rocketsState) => {
					for (let i = 0, len = rocketsState.size; i < len; i++) {
						const rocket = rocketsState.get(i);
						const rx = rocket.get("x");
						const ry = rocket.get("y");
						const destructionTime = rocket.get("destructionTime");
						let isDestroyed = !!destructionTime;

						if (!isDestroyed) {
							if (ry < -ROCKET_SIZE) {
								clearRockets[i] = true;
							} else {
								for (let j = 0, lenj = enemies.size; j < lenj; j++) {
									const enemy = enemies.get(j);
									const ex = enemy.get("x");
									const ey = enemy.get("y");
									const eDestroyed = !!enemy.get("destructionTime");

									if (!eDestroyed &&
										ry <= ey &&
										ey - ENEMY_HALF_SIZE <= ry + ROCKET_HALF_SIZE &&
										rx - ROCKET_HALF_SIZE <= ex + ENEMY_HALF_SIZE &&
										ex - ENEMY_HALF_SIZE <= rx + ROCKET_HALF_SIZE
									) {
										isDestroyed = true;

										rocketsState.setIn([i, "destructionTime"], true);
										enemies = enemies.setIn([j, "destructionTime"], gameTime);
										state.set("destroyedEnemies", state.get("destroyedEnemies") + 1);

										break;
									}
								}

								if (!isDestroyed) {
									rocketsState.setIn([i, "y"], ry - (ROCKET_SPEED * delta));
								}
							}
						}
					}
				});

				// ENEMIES
				const px = state.getIn(["player", "x"]);
				const py = state.getIn(["player", "y"]);
				enemies = enemies.withMutations((enemiesState) => {
					for (let i = 0, len = enemiesState.size; i < len; i++) {
						const enemy = enemiesState.get(i);
						const ex = enemy.get("x");
						const ey = enemy.get("y");
						const destructionTime = enemy.get("destructionTime");
						let isDestroyed = !!destructionTime;

						if (isDestroyed) {
							if (destructionTime + EXPLOSION_DURATION <= gameTime) {
								enemiesState.setIn([i, "destructionTime"], true);
								clearEnemies[i] = true;
							}
						} else {
							if (py <= ey + ENEMY_SIZE &&
								ey <= py + PLAYER_SIZE &&
								px - PLAYER_HALF_SIZE <= ex + ENEMY_HALF_SIZE &&
								ex - ENEMY_HALF_SIZE <= px + PLAYER_HALF_SIZE
							) {
								isDestroyed = true;

								enemiesState.setIn([i, "destructionTime"], gameTime);
								state.setIn(["player", "destructionTime"], gameTime);
								state.set("destroyedEnemies", state.get("destroyedEnemies") + 1);
							}

							if (!isDestroyed) {
								const step = enemy.get("speed") * delta;
								const change = enemy.get("y") + step;

								if (change > 1 + ENEMY_SIZE) {
									if (!clearEnemies[i]) {
										clearEnemies[i] = true;
										state.set("passedEnemies", state.get("passedEnemies") + 1);
									}
								} else {
									enemiesState.setIn([i, "y"], change);
								}
							}
						}
					}
				});

				// CLEAN UP
				if (lastCleanUpTime + CLEAN_UP_INTERVAL <= gameTime) {
					let deleted = 0;
					for (let index in clearEnemies) {
						enemies = enemies.delete(index - deleted++);
					}
					deleted = 0;
					for (let index in clearRockets) {
						rockets = rockets.delete(index - deleted++);
					}
					clearEnemies = {};
					clearRockets = {};
					lastCleanUpTime = gameTime;
				}

				state.set("rockets", rockets);
				state.set("enemies", enemies);
			});

		case ACTIONS.GAME_START:
			return state.withMutations((state) => {
				state.set("gameTime", 0);
				state.set("gameOver", false);
				state.set("paused", false);
				state.set("score", 0);
				state.set("player", Map({
					size: PLAYER_SIZE,
					y: 0.9,
					x: 0.5,
					movement: NOT_MOVING
				}));
				state.set("passedEnemies", 0);
				state.set("destroyedEnemies", 0);
				state.set("rocketsFired", 0);
				state.set("enemies", List());
				state.set("rockets", List());
			});

		case ACTIONS.GAME_QUIT:
		case ACTIONS.GAME_OVER:
			return state.withMutations((state) => {
				state.delete("player");
				state.delete("enemies");
				state.delete("rockets");

				if (action.type === ACTIONS.GAME_OVER) {
					state.set("gameOver", true);
					state.set("paused", true);
					state.set("score", state.get("passedEnemies") + (state.get("destroyedEnemies") * 10));
				}
			});

		case ACTIONS.CLEAR_SCORE:
			return state.set("score", 0);

		case ACTIONS.PLAYER_MOVE_IDLE:
			return state.setIn(["player", "movement"], NOT_MOVING);

		case ACTIONS.PLAYER_MOVE_LEFT:
			return state.setIn(["player", "movement"], -1);

		case ACTIONS.PLAYER_MOVE_RIGHT:
			return state.setIn(["player", "movement"], 1);

		case ACTIONS.PLAYER_SHOOT:
			let player = state.get("player");
			let rockets = state.get("rockets").push(Map({
				id: rocketId++,
				x: player.get("x"),
				y: player.get("y") - PLAYER_HALF_SIZE,
				size: ROCKET_SIZE
			}));

			return state.withMutations((state) => {
				state.set("rocketsFired", state.get("rocketsFired") + 1);
				state.set("rockets", rockets);
			});

		case ACTIONS.SPAWN_ENEMY:
			return state.set("enemies", state.get("enemies").withMutations((state) => {
				for (let i = 0; i < data; i++) {
					const enemy = Map({
						id: enemyId++,
						size: ENEMY_SIZE,
						x: Math.random(),
						y: -ENEMY_SIZE,
						speed: ENEMY_SPEED + ((Math.random() - 0.5) * ENEMY_SPEED)
					});
					state.push(enemy);
				}
			}));
	}

	return state;
}
