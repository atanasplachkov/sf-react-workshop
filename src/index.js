import React from "react";
import ReactDOM from "react-dom";

import Root from "./components/root";
import "./main.scss";

if (module.hot) {
	module.hot.accept();
}

ReactDOM.render(
	<Root/>,
	document.getElementById("root")
);
