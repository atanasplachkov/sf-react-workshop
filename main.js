"use strict";

const http = require("http");
const express = require("express");
const webpack = require("webpack");
const wpDevMiddleware = require("webpack-dev-middleware");
const wpHotMiddleware = require("webpack-hot-middleware");
const ReactDOMServer = require("react-dom/server");

const isProduction = process.env.NODE_ENV === "production";

const app = express();
const webpackConfig = require("./webpack.config");
const bundler = webpack(webpackConfig);

app.use(wpDevMiddleware(bundler, {
	noInfo: true
}));

app.use(wpHotMiddleware(bundler, {
	path: "/__webpack_hmr",
	heartbeat: 10 * 1000
}));

app.use(express.static("node_modules"));
app.use("/public", express.static("src/static"));

const scores = require("./src/static/highscores.json");
const React = require("react");
require("babel-register")();
const HighScoresList = require("./src/components/highscores-list").default;
const sendHighScores = function (res) {
	res.send(ReactDOMServer.renderToString(
		React.createElement(HighScoresList, {
			scores: scores
		})
	));
};
app.get("/api/highscores", function (req, res) {
	sendHighScores(res);
});
app.post("/api/highscores", function (req, res) {
	let data = "";
	req.setEncoding("utf8");
	req.on("data", function (chunk) {
		data += chunk;
	});
	req.on("end", function () {
		try {
			data = JSON.parse(data);

			let name = data.name;
			let score = data.score;
			let isInserted = false;
			for (let i = 0, len = scores.length; i < len; i++) {
				if (score >= scores[i].score) {
					scores.splice(i, 0, {name, score});
					isInserted = true;
					break;
				}
			}
			if (scores.length > 15) {
				scores.splice(15);
			} else if (!isInserted) {
				scores.push({name, score});
			}
			sendHighScores(res);
		} catch (e) {
			res.status(400).end();
		}
	});
});

const indexFile = __dirname + "/src/static/" + (isProduction ? "index_prod.html" : "index.html");
app.get("/*", function (req, res) {
	res.sendFile(indexFile);
});

const server = http.createServer(app);
server.listen(process.env.PORT || 3000, function () {
	console.log("Listening on %j", server.address().port);
});
