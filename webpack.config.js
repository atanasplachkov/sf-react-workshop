const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const isProduction = process.env.NODE_ENV === "production";
const loaders = [
	{
		test: /\.js$/,
		loader: "babel-loader",
		exclude: /node_modules/
	}
];
const plugins = [];

if (isProduction) {
	const extractSass = new ExtractTextPlugin({
		filename: "public/styles.css",
		disable: process.env.NODE_ENV === "development"
	});

	loaders.push({
		test: /\.scss$/,
		loader: extractSass.extract({
			fallback: "style-loader",
			use: "css-loader?sourceMap!sass-loader?sourceMap"
		})
	});
	plugins.push(extractSass);
} else {
	loaders.push({
		test: /\.scss$/,
		loader: ["style-loader", "css-loader", "sass-loader"]
	});
	plugins.push(new webpack.HotModuleReplacementPlugin());
}

module.exports = {
	entry: [
		"webpack-hot-middleware/client?path=/__webpack_hmr&timeout=5000",
		"./src/index.js"
	],
	output: {
		path: __dirname,
		filename: "public/bundle.js"
	},
	externals: {
		"immutable": "Immutable",
		"redux": "Redux",
		"react": "React",
		"react-dom": "ReactDOM",
		"react-redux": "ReactRedux"
	},
	devtool: "source-map",
	module: {
		loaders: loaders
	},
	plugins: plugins
};
